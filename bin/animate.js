#!/usr/bin/env node
"use strict"

const fs = require('fs').promises
const getopts = require("getopts")
const animator = require('..')

const options = getopts(process.argv.slice(2), {
  alias: {
    help: "h",
    fps: "f",
    "keep-duplicates": "d",
    output: "o",
  },
  default: {
    fps: 30,
    "keep-duplicates": false,
    output: "-",
    svgo: true
  },
  strings: ["fps", "output"],
  bools: ["keep-duplicates", "svgo"]
})

if (options.help || options._.length === 0) {
  console.log('usage: animate [-f|--fps <integer>] [-o|--output <file>] [-d|--keep-duplicates] [--no-svgo] <input files>...')
  process.exit(0)
}

async function outputImage() {
  const output = await animator(options._, {fps: options.fps,
                                            keep_duplicates: options['keep-duplicates'],
                                            svgo: options.svgo})
  if (options.output == '-')
    console.log(output)
  else
    await fs.writeFile(options.output, output)
}

outputImage()
