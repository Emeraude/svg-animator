"use strict"

const fs = require('fs').promises
const cheerio = require('cheerio')
const svgo = require('svgo')

function removeDuplicates(Out) {
  const seen = {}
  Out('[id]').clone().each(function(_, e) {
    const id = e.attribs.id
    delete e.attribs.id
    const html = Out.html(e)
    if (seen[html] !== undefined) {
      Out('[href="#' + id + '"]').each(function(_, e) {
        e.attribs.href = '#' + seen[html]
      })
      Out('[xlink\\:href="#' + id + '"]').each(function(_, e) {
        e.attribs['xlink:href'] = '#' + seen[html]
      })
      Out('[fill="url(#' + id + ')"]').each(function(_, e) {
        e.attribs.fill = 'url(#' + seen[html] + ')'
      })
      Out('[id="' + id + '"]').remove()
    } else {
      seen[html] = id
    }
  })
  return Out
}

module.exports = async function(files, options = {}) {
  const Out = cheerio.load('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  version="1.1"><defs></defs></svg>', {xmlMode: true})
  options.fps = options.fps || 30
  options.keep_duplicates = options.keep_duplicates || false
  options.svgo == options.svgo || false
  const imageSize = {w: 0, h: 0}
  let allIds = ''

  for (const i in files) {
    const svg = await fs.readFile(files[i])
    const Svg = cheerio.load(svg, {xmlMode: true})
    const id = 'import-' + i + '-x'
    allIds += '#' + id + ';'
    imageSize.w = Math.max(parseFloat(Svg('svg:nth-child(1)').attr('width')), imageSize.w)
    imageSize.h = Math.max(parseFloat(Svg('svg:nth-child(1)').attr('height')), imageSize.h)
    Svg('[id]').each(function(_, e) {
      e.attribs.id += '-' + i
    })
    Svg('[href^="#"]').each(function(_, e) {
      e.attribs.href += '-' + i
    })
    Svg('[xlink\\:href^="#"]').each(function(_, e) {
      e.attribs['xlink:href'] += '-' + i
    })
    Svg('[fill^="url(#"]').each(function(_, e) {
      e.attribs.fill = e.attribs.fill.replace(')', '-' + i + ')')
    })
    Out('defs').append('<g id="' + id + '"></g>')
    Out('defs > g:last-child').append(Svg('svg > *:not(defs)'))
    Out('defs').append(Svg('defs > *'))
  }

  Out('svg:nth-child(1)').append('<use xlink:href="#import-0-x">'
                                 + '<animate attributeName="xlink:href" repeatCount="indefinite" dur="' + parseFloat((files.length / options.fps).toFixed(3)) + 's" begin="0s" values="' + allIds.slice(0, -1) + '" />'
                                 + '</use>')

  Out('svg:nth-child(1)').attr('width', imageSize.w)
    .attr('height', imageSize.h)

  if (options.keep_duplicates !== true) {
    removeDuplicates(Out)
  }
  const xml = Out.xml()
  if (options.svgo === true) {
    const result = svgo.optimize(xml, {
      'plugins': [
        'removeDoctype',
        'removeXMLProcInst',
        'removeComments',
        'removeMetadata',
        'removeEditorsNSData',
        'cleanupAttrs',
        'mergeStyles',
        'inlineStyles',
        'minifyStyles',
        // 'cleanupIDs',
        'removeUselessDefs',
        'cleanupNumericValues',
        'convertColors',
        'removeUnknownsAndDefaults',
        'removeNonInheritableGroupAttrs',
        'removeUselessStrokeAndFill',
        'removeViewBox',
        'cleanupEnableBackground',
        'removeHiddenElems',
        'removeEmptyText',
        'convertShapeToPath',
        'convertEllipseToCircle',
        'moveElemsAttrsToGroup',
        'moveGroupAttrsToElems',
        'collapseGroups',
        'convertPathData',
        'convertTransform',
        'removeEmptyAttrs',
        'removeEmptyContainers',
        'mergePaths',
        'removeUnusedNS',
        'sortDefsChildren',
        'removeTitle',
        'removeDesc'
      ],
      'multipass': true
    })
    return result.data
  }
  return xml
}
