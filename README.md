# SVG Animator

Concatenate several svg images into a single animated one.  
Note that this is a WiP tool, which generates relatively heavy svg files. You might want to hand edit some parts of the output file.

## Usage

```sh
./bin/animate.js [-f|--fps <integer>] [-o|--output <file>] [-d|--keep-duplicates] [--no-svgo] <input files>...
```

## Examples

Given the examples files `1.svg`, `2.svg`, `3.svg`, `4.svg`, `5.svg`, `6.svg`, `7.svg`, `8.svg`, `9.svg` `10.svg` `11.svg` located in [examples](./examples), here are some examples:

![1.svg](./examples/1.svg)![2.svg](./examples/2.svg)![2.svg](./examples/3.svg)![3.svg](./examples/4.svg)![4.svg](./examples/5.svg)![5.svg](./examples/6.svg)![6.svg](./examples/7.svg)![7.svg](./examples/8.svg)![8.svg](./examples/9.svg)![9.svg](./examples/10.svg)![10.svg](./examples/11.svg)![2.svg](./examples/11.svg)


```sh
./bin/animate.js -f 32 1.svg 2.svg 3.svg 4.svg 5.svg 6.svg 7.svg 8.svg 9.svg 10.svg 11.svg
```

![32 fps example](./examples/out-32.svg)

```sh
./bin/animate.js -f 10 1.svg 2.svg 3.svg 4.svg 5.svg 6.svg 7.svg 8.svg 9.svg 10.svg 11.svg
```

![10 fps example](./examples/out-10.svg)

## Optimizations

The two options `--keep-duplicates` and `--no-svgo` are only available if you want to remove optimisations. The first one do not check for duplicates elements (which happen quite often) and the second one disable the use of [svgo](https://github.com/svg/svgo/), a tool that reduces svg sizes.  
Here is a table that shows the result for the examples shown above:

| File | Output size (bytes) | Output size (human readable) |
| --- | --- | --- |
| All of the one frame images | 30230 | 30KiB |
| 32 fps example without duplicate, with svgo (default) | 12645 | 13KiB |
| 32 fps example with duplicates, with svgo | 29477 | 29KiB |
| 32 fps example with duplicated, without svgo | 29600 | 29KiB |

Note that the use of *svgo* doesn't seem to make any difference in this example. That's because the input files have already been compressed by *svgo*. You'll see a way bigger difference using uncompressed files.

Example assets come from the game "Hammerfest", created by Motion Twin and licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)
